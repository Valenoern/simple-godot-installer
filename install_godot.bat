@echo off
set godothome=https://downloads.tuxfamily.org/godotengine
set godoturl="%godothome%/3.2/Godot_v3.2-stable_win64.exe.zip"
rem set godoturl="https://www.w3.org/TR/PNG/iso_8859-1.txt"
rem ^ that is for testing the download with a much smaller file
set godotpath=C:\Program Files\Godot
set savegodot=n

rem escape "Program Files" by using the dos shortname
set godotpathtrue=C:\Progra~1\Godot
rem ah, windows.... puts spaces in every filename,
rem then refuses to let you use any filenames with spaces or reliably escape them

rem use THIS directory, otherwise we always start in c:\system32.
rem yes, really.
cd "%~dp0"
set godotexe=.\godot.exe
rem windows in fact allows .\ as the current directory..... sometimes.


echo This batch file should be 'Run as Administrator'.
echo Right-click, and read it in notepad before running if you're concerned

:MAKEFOLDER

if exist "%godotpathtrue%\godot.exe" (
  echo Godot binary already installed in %godotpath%
  goto PATHENTRY
)

if exist "%godotpathtrue%" (
  echo %godotpath% already created
  goto DOWNLOAD
)

echo Creating %godotpath%
mkdir "%godotpathtrue%"

:DOWNLOAD

if exist "%godotexe%" goto MOVEGODOT
if exist "%godotexe%.zip" goto UNZIP

set /P savegodot="Download godot 3.2 from godotengine.org? y/n "

if "%savegodot%"=="y" (
  start /wait curl "%godoturl%" -o godot.exe.zip
)

:UNZIP

echo Unzipping godot
echo If this does not work, unzip it manually to "godot.exe" and restart this installer

powershell.exe -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::ExtractToDirectory('godot.exe.zip', '.'); }"
rename Godot*.exe godot.exe

rem again it's time to gripe about windows
rem for not having a built in unzip command until very recently
rem this command is claimed to work since windows 8 with powershell 3
rem https://stackoverflow.com/questions/17546016/24323413

:MOVEGODOT

if not exist "%godotpathtrue%" (
  echo %godotpath% was not created, cannot put godot there
  goto End
)

if not exist "%godotexe%" (
  echo No godot.exe in this folder to move to %godotpath% .
  echo Make sure you have one, or maybe move it there manually
  goto PATHENTRY
)

echo Moving godot to %godotpath%
move "%godotexe%" "%godotpathtrue%"

:PATHENTRY

rem "where" is similar to locate on *nix, giving the path of a command
rem if given /q it returns only an exit code
rem 0 if the command was found, 1 if it was not
where /q godot.exe

if "%errorlevel%"=="0" (
  echo Godot is already in $PATH
  echo You can already open any scene file as "godot scene.tscn"
  goto End
)

echo Adding godot.exe to system $PATH
set pathexpression="%path%;%godotpath%"
setx path %pathexpression%
rem if this doesn't work you should press Win+R and open systempropertiesadvanced
rem under User Variables, edit the Path variable and "add new" C:\Program Files\Godot

echo You may now open any scene file anywhere as "godot scene.tscn"

:End
pause